import React,{Component} from 'react';

class Header extends  Component{
    render() {
        return(
            <div className="container">
                <div className="row">
                    <form>
                        <div className="form-row">
                            <div className="col-7">
                                <input type="text" className="form-control" placeholder="City"/>
                            </div>
                            <div className="col">
                                <input type="text" className="form-control" placeholder="State"/>
                            </div>
                            <div className="col">
                                <input type="text" className="form-control" placeholder="Zip"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Header;