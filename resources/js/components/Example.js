import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Company from "./Company";
import Header from "./Header";

class Example extends Component{
    render(){
        return (
            <div>
                <Header/>
                <Company/>
            </div>

        );
    }
}
export default Example;
if (document.getElementById('app')) {
    ReactDOM.render(<Example />, document.getElementById('app'));
}
